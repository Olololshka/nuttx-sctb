#/bin/sh

update_branch_from() {
	local branch=$1
	local repo_addr=$2
	local temp_repo='temp_repo'
	local current_branch=`git branch | grep \* | awk '{print $2}'`
	
	git branch $branch
	git remote add $temp_repo $repo_addr
	git checkout $branch
	git pull $temp_repo master
	git checkout $current_branch
	git remote remove $temp_repo
}

cd nuttx; update_branch_from mainstream_master https://Olololshka@bitbucket.org/patacongo/nuttx.git

cd Documentation; git checkout master; git pull; cd ..
cd arch; git checkout master; git pull; cd ..

cd configs; update_branch_from mainstream_master https://Olololshka@bitbucket.org/nuttx/boards.git

cd ../../apps ; update_branch_from mainstream_master https://Olololshka@bitbucket.org/nuttx/apps.git

echo "## READY mainstream_master updated"

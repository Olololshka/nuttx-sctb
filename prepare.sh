#/bin/sh

git submodule update --init --recursive

cd nuttx; git branch SCTB_master origin/SCTB_master; git checkout SCTB_master
cd ../../apps ; git branch SCTB_master origin/SCTB_master; git checkout SCTB_master

echo "## READY to work"
